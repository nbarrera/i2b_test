/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.i2b_test.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@Entity
@Table(name = "reservation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reservation.findAll", query = "SELECT r FROM Reservation r"),
    @NamedQuery(name = "Reservation.findById", query = "SELECT r FROM Reservation r WHERE r.id = :id"),
    @NamedQuery(name = "Reservation.findByRegisterDate", query = "SELECT r FROM Reservation r WHERE r.registerDate = :registerDate"),
    @NamedQuery(name = "Reservation.findByProgrammedDate", query = "SELECT r FROM Reservation r WHERE r.programmedDate = :programmedDate")})
public class Reservation implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @Column(name = "register_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registerDate;
    @Column(name = "programmed_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date programmedDate;
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Client clientId;
    @JoinColumn(name = "film_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Film filmId;
    
    public Reservation() {
        clientId = new Client();
        filmId = new Film();
    }
    
    public Reservation(Long id) {
        this.id = id;
        clientId = new Client();
        filmId = new Film();
    }
    
    public Reservation(Long id, Date registerDate) {
        this.id = id;
        this.registerDate = registerDate;
        clientId = new Client();
        filmId = new Film();
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Date getRegisterDate() {
        return registerDate;
    }
    
    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }
    
    public Date getProgrammedDate() {
        return programmedDate;
    }
    
    public void setProgrammedDate(Date programmedDate) {
        this.programmedDate = programmedDate;
    }
    
    public Client getClientId() {
        return clientId;
    }
    
    public void setClientId(Client clientId) {
        this.clientId = clientId;
    }
    
    public Film getFilmId() {
        return filmId;
    }
    
    public void setFilmId(Film filmId) {
        this.filmId = filmId;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservation)) {
            return false;
        }
        Reservation other = (Reservation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Cliente: " + clientId.getName() + " | Pelicula: " + filmId.getName();
    }
    
    public void resetClient() {
        String identificationNumber = clientId.getIdentificationnumber();
        clientId = new Client();
        clientId.setIdentificationnumber(identificationNumber);
    }
    
}
